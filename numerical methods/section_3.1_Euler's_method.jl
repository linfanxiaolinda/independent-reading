# Example 3.1.1
function approx(f, h, n,
     x0, y0)
    i = 0
    y1 = y0
    while i <= n
        y1 = y0 + h * f(x0, y0)
        println(string("x" * string(i) * "=" * string(x0) * ", "
        * "y" * string(i) * "=" * string(y0)))
        y0 = y1
        x0 += h
        i += 1
    end
    return y0
end
# 0.1+0.1+0.1 returns 0.300000000004

function approx(f, h, n, x0, y0)
    # make a list of x-values I want to keep
    xList = collect(x0:h:(x0+h*n))
    i = 0
    y1 = y0
    while i <= n
        x0 = xList[i+1]
        y1 = y0 + h * f(x0, y0)
        println(string("x" * string(i) * "=" * string(x0) * ", "
        * "y" * string(i) * "=" * string(y0)))
        y0 = y1
        i += 1
    end
    return y1
end

# y' as a function of x and y
function f(x,y)
    -2*y + x^3 * e^(-2*x)
end

approx(f, 0.1, 3, 0, 1)

# Example 3.1.2
xSelected = collect(0:0.1:1.0)
function approx(f, h, n, x0, y0)
    xList = collect(x0:h:(x0+h*n))
    i = 0
    y1 = y0
    while i <= n
        x0 = xList[i+1]
        y1 = y0 + h * f(x0, y0)
        if x0 in xSelected
            println(string("x" * string(i) * "=" * string(x0) * ", " * "y" * string(i) * "=" * string(y0)))
        end
        y0 = y1
        i += 1
    end
    return y1
end

approx(f, 0.1, 10, 0, 1)
approx(f, 0.05, 20, 0, 1)
approx(f, 0.025, 40, 0, 1)

function exact(x)
    e^(-2*x)/4*(x^4 + 4)
end
for x in xSelected
    println(string("x" * "=" * string(x) * ", " * "y" * "=" * string(exact(x))))
end

# Example 3.1.3
function f(x,y)
    -2*y^2 + x*y + x^2
end
approx(f, 0.1, 10, 0, 1)
approx(f, 0.05, 20, 0, 1)
approx(f, 0.025, 40, 0, 1)

# Example 3.1.4
function f(x,y)
    1 + 2*x*y
end

# Euler's method
xSelected = collect(0:0.2:2.0)
function approx(f, h, n, x0, y0)
    xList = collect(x0:h:(x0+h*n))
    i = 0
    y1 = y0
    while i <= n
        x0 = xList[i+1]
        y1 = y0 + h * f(x0, y0)
        if x0 in xSelected
            println(string("x" * string(i) * "=" * string(x0) * ", " * "y" * string(i) * "=" * string(y0)))
        end
        y0 = y1
        i += 1
    end
    return y1
end

approx(f, 0.2, 10, 0, 3)
approx(f, 0.1, 20, 0, 3)
approx(f, 0.05, 40, 0, 3)

# Euler's semilinear method
#find approximations for u
function y_1(x)
    e^(x^2)
end

function u_deriv(x,u)
    e^(-x^2)
end

approx(u_deriv, 0.2, 10, 0, 3)

xSelected = collect(0:0.2:2.0)
function variation_of_parameters(uDeriv, h, n, x0, y0, u0)
    xList = collect(x0:h:(x0+h*n))
    i = 0
    while i <= n
        x0 = xList[i+1]
        y0 = u0 * y_1(x0)
        if x0 in xSelected
            println(string("x" * string(i) * "=" * string(x0) * ", " * "y" * string(i) * "=" * string(y0) * ", " * "u" * string(i) * "=" * string(u0)))
        end
        u1 = u0 + h * uDeriv(x0, u0)
        u0 = u1
        i += 1
    end
    return y0
end

variation_of_parameters(u_deriv, 0.2, 10, 0, 3, 3)
variation_of_parameters(u_deriv, 0.1, 20, 0, 3, 3)
variation_of_parameters(u_deriv, 0.05, 40, 0, 3, 3)

# Example 3.1.5
# Euler's method
xSelected = collect(1.0:0.1:2.0)
function approx(f, h, n, x0, y0)
    xList = collect(x0:h:(x0+h*n))
    i = 0
    y1 = y0
    while i <= n
        x0 = xList[i+1]
        y1 = y0 + h * f(x0, y0)
        if x0 in xSelected
            println(string("x" * string(i) * "=" * string(x0) * ", " * "y" * string(i) * "=" * string(y0)))
        end
        y0 = y1
        i += 1
    end
    return y1
end

function f(x,y)
    2*y + x/(1+y^2)
end

approx(f, 0.1, 10, 1, 7)
approx(f, 0.05, 20, 1, 7)
approx(f, 0.025, 40, 1, 7)

# Euler's semilinear method
function y_1(x)
    e^(2*x)
end

function u_deriv(x,u)
    (x*e^(-2*x))/(1+u^2*e^(4*x))
end

approx(u_deriv, 0.1, 10, 1, 7*e^(-2))

xSelected = collect(1.0:0.1:2.0)
function variation_of_parameters(uDeriv, h, n, x0, y0, u0)
    xList = collect(x0:h:(x0+h*n))
    i = 0
    while i <= n
        x0 = xList[i+1]
        y0 = u0 * y_1(x0)
        if x0 in xSelected
            println(string("x" * string(i) * "=" * string(x0) * ", " * "y" * string(i) * "=" * string(y0) * ", " * "u" * string(i) * "=" * string(u0)))
        end
        u1 = u0 + h * uDeriv(x0, u0)
        u0 = u1
        i += 1
    end
    return y0
end

variation_of_parameters(u_deriv, 0.1, 10, 1, 7, 7*e^(-2))
variation_of_parameters(u_deriv, 0.05, 20, 1, 7, 7*e^(-2))
variation_of_parameters(u_deriv, 0.025, 40, 1, 7, 7*e^(-2))

# Example 3.1.6
# Euler's method
xSelected = collect(2.0:0.1:3.0)
function approx(f, h, n, x0, y0)
    xList = collect(x0:h:(x0+h*n))
    i = 0
    y1 = y0
    while i <= n
        x0 = xList[i+1]
        y1 = y0 + h * f(x0, y0)
        if x0 in xSelected
            println(string("x" * string(i) * "=" * string(x0) * ", " * "y" * string(i) * "=" * string(y0)))
        end
        y0 = y1
        i += 1
    end
    return y1
end

function f(x,y)
    -3*x^2*y + y^2 + 1
end

approx(f, 0.1, 10, 2, 2)
approx(f, 0.05, 20, 2, 2)
approx(f, 0.025, 40, 2, 2)

# Euler's semilinear method
function y_1(x)
    e^(-x^3)
end

function u_deriv(x,u)
    e^(x^3)*(1+u^2*e^(-2*x^3))
end

approx(u_deriv, 0.1, 10, 2, 2*e^8)

xSelected = collect(2.0:0.1:3.0)
function variation_of_parameters(uDeriv, h, n, x0, y0, u0)
    xList = collect(x0:h:(x0+h*n))
    i = 0
    while i <= n
        x0 = xList[i+1]
        y0 = u0 * y_1(x0)
        if x0 in xSelected
            println(string("x" * string(i) * "=" * string(x0) * ", " * "y" * string(i) * "=" * string(y0) * ", " * "u" * string(i) * "=" * string(u0)))
        end
        u1 = u0 + h * uDeriv(x0, u0)
        u0 = u1
        i += 1
    end
    return y0
end

variation_of_parameters(u_deriv, 0.1, 10, 2, 2, 2*e^8)
variation_of_parameters_specified_x(y_1, u_deriv, 0.1, 10, 2, 2, 2*e^8, collect(2.0:0.1:3.0))
variation_of_parameters(u_deriv, 0.05, 20, 2, 2, 2*e^8)
variation_of_parameters(u_deriv, 0.025, 40, 2, 2, 2*e^8)

# Exercises --------------------------------------------------------------------
# Euler's method
function euler(f, h, n, x0, y0)
    xList = collect(x0:h:(x0+h*n))
    i = 0
    y1 = y0
    while i <= n
        x0 = xList[i+1]
        y1 = y0 + h * f(x0, y0)
        println(string("x" * string(i) * "=" * string(x0) * ", " * "y" * string(i) * "=" * string(y0)))
        y0 = y1
        i += 1
    end
    return y1
end

# Euler's method with specified x-values
function euler_specified_x(f, h, n, x0, y0, xSelected)
    xList = collect(x0:h:(x0+h*n))
    i = 0
    y1 = y0
    while i <= n
        x0 = xList[i+1]
        y1 = y0 + h * f(x0, y0)
        if x0 in xSelected
            println(string("x" * string(i) * "=" * string(x0) * ", " * "y" * string(i) * "=" * string(y0)))
        end
        y0 = y1
        i += 1
    end
    return y1
end

# Euler's method with specified x-values that prints residuals
function euler_specified_x_with_residuals(f, h, n, x0, y0, xSelected, R)
    xList = collect(x0:h:(x0+h*n))
    i = 0
    y1 = y0
    while i <= n
        x0 = xList[i+1]
        y1 = y0 + h * f(x0, y0)
        if x0 in xSelected
            println(string("x" * string(i) * "=" * string(x0) * ", " * "y" * string(i) * "=" * string(y0)) * ", " * "R = " * string(R(x0,y0)))
        end
        y0 = y1
        i += 1
    end
    return y1
end

# Euler's semilinear method
# estimate y from u
function variation_of_parameters_specified_x(y_1, uDeriv, h, n, x0, y0, u0, xSelected)
    xList = collect(x0:h:(x0+h*n))
    i = 0
    while i <= n
        x0 = xList[i+1]
        y0 = u0 * y_1(x0)
        if x0 in xSelected
            println(string("x" * string(i) * "=" * string(x0) * ", " * "y" * string(i) * "=" * string(y0) * ", " * "u" * string(i) * "=" * string(u0)))
        end
        u1 = u0 + h * uDeriv(x0, u0)
        u0 = u1
        i += 1
    end
    return y0
end

# Exercise 3.1.4
function f(x,y)
    (1+x)/(1-y^2)
end

euler(f, 0.1, 3, 2, 3)

# Exercise 3.1.5
function f(x,y)
    -x^2*y + sin(x*y)
end

euler(f, 0.2, 3, 1, π)

# Exercise 3.1.8
function f(x,y)
    (y^2+x*y-x^2)/(x^2)
end

euler_specified_x(f, 0.05, 10, 1, 2, collect(1.0:0.05:1.5))
euler_specified_x(f, 0.025, 20, 1, 2, collect(1.0:0.05:1.5))
euler_specified_x(f, 0.0125, 40, 1, 2, collect(1.0:0.05:1.5))

function exact(x)
    (x*(1+x^2/3))/(1-x^2/3)
end

exact(1.5)

# Exercise 3.1.9
function f(x,y)
    (2*x+1)/(5*y^4+1)
end

# residuals
function R(x,y)
    y^5 + y - x^2 - x + 4
end

euler_specified_x_with_residuals(f, 0.1, 10, 2, 1, collect(2.0:0.1:3.0), R)
euler_specified_x_with_residuals(f, 0.05, 20, 2, 1, collect(2.0:0.1:3.0), R)
euler_specified_x_with_residuals(f, 0.025, 40, 2, 1, collect(2.0:0.1:3.0), R)

# Exercise 3.1.13
# Euler's method
function f(x,y)
    7*e^(-3*x) - 3*y
end

euler_specified_x(f, 0.1, 10, 0, 6, collect(0.0:0.1:1.0))
euler_specified_x(f, 0.05, 20, 0, 6, collect(0.0:0.1:1.0))
euler_specified_x(f, 0.025, 40, 0, 6, collect(0.0:0.1:1.0))

# Euler's semilinear method
function y_1(x)
    e^(-3x)
end

function u_deriv(x,u)
    7
end

# u'' = 0

euler(u_deriv, 0.1, 10, 0, 6)

variation_of_parameters_specified_x(y_1, u_deriv, 0.1, 10, 0, 6, 6, collect(0.0:0.1:1.0))
variation_of_parameters_specified_x(y_1, u_deriv, 0.05, 20, 0, 6, 6, collect(0.0:0.1:1.0))
variation_of_parameters_specified_x(y_1, u_deriv, 0.025, 40, 0, 6, 6, collect(0.0:0.1:1.0))

function exact(x)
    e^(-3*x) * (7*x+6)
end
for x in collect(0.0:0.1:1.0)
    println(string("x" * "=" * string(x) * ", " * "y" * "=" * string(exact(x))))
end

# u'' = 0, so Euler's semilinear method gives the exact solution because the local truncation error is 0, so is the global truncation error, which depends on the local error

# Exercise 14
# Euler's method
function f(x,y)
    2*y + 1/(1+x^2)
end

euler_specified_x(f, 0.1, 10, 2, 2, collect(2.0:0.1:3.0))
euler_specified_x(f, 0.05, 20, 2, 2, collect(2.0:0.1:3.0))
euler_specified_x(f, 0.025, 40, 2, 2, collect(2.0:0.1:3.0))

# Euler's semilinear method
function y_1(x)
    e^(2*x)
end

function u_deriv(x,u)
    1/((1+x^2)*(e^(2*x)))
end

euler_specified_x(u_deriv, 0.1, 10, 2, 2/(e^4), collect(2.0:0.1:3.0))

variation_of_parameters_specified_x(y_1, u_deriv, 0.1, 10, 2, 2, 2/(e^4), collect(2.0:0.1:3.0))
variation_of_parameters_specified_x(y_1, u_deriv, 0.05, 20, 2, 2, 2/(e^4), collect(2.0:0.1:3.0))
variation_of_parameters_specified_x(y_1, u_deriv, 0.025, 40, 2, 2, 2/(e^4), collect(2.0:0.1:3.0))

# Exercise 3.1.16
# Euler's method
function f(x,y)
    -y/x + sin(x)/(x^2)
end

euler_specified_x(f, 0.2, 10, 1, 2, collect(1.0:0.2:3.0))
euler_specified_x(f, 0.1, 20, 1, 2, collect(1.0:0.2:3.0))
euler_specified_x(f, 0.05, 40, 1, 2, collect(1.0:0.2:3.0))

# Euler's semilinear method
function y_1(x)
    1/x
end

function u_deriv(x,u)
    sin(x)/x
end

variation_of_parameters_specified_x(y_1, u_deriv, 0.2, 10, 1, 2, 2, collect(1.0:0.2:3.0))
variation_of_parameters_specified_x(y_1, u_deriv, 0.1, 20, 1, 2, 2, collect(1.0:0.2:3.0))
variation_of_parameters_specified_x(y_1, u_deriv, 0.05, 40, 1, 2, 2, collect(1.0:0.2:3.0))

# Exercise 3.1.18
# Euler's method
function f(x,y)
    e^x/((1+x^2))^2 - 2*x*y/(1+x^2)
end

euler_specified_x(f, 0.2, 40, 0, 1, collect(0.0:0.2:2.0))
euler_specified_x(f, 0.1, 80, 0, 1, collect(0.0:0.2:2.0))
euler_specified_x(f, 0.05, 160, 0, 1, collect(0.0:0.2:2.0))

# Euler's semilinear method
function y_1(x)
    1/(1+x^2)
end

function u_deriv(x,u)
    (e^x)/(1+x^2)
end

variation_of_parameters_specified_x(y_1, u_deriv, 0.2, 10, 0, 1, 1, collect(0.0:0.2:2.0))
variation_of_parameters_specified_x(y_1, u_deriv, 0.1, 20, 0, 1, 1, collect(0.0:0.2:2.0))
variation_of_parameters_specified_x(y_1, u_deriv, 0.05, 40, 0, 1, 1, collect(0.0:0.2:2.0))

# Exercise 3.1.21
# Euler's method
function f(x,y)
    x/(y^2*(y+1))+4*y
end

euler_specified_x(f, 0.1, 10, 0, 1, collect(0.0:0.1:1.0))
euler_specified_x(f, 0.05, 20, 0, 1, collect(0.0:0.1:1.0))
euler_specified_x(f, 0.025, 40, 0, 1, collect(0.0:0.1:1.0))

# Euler's semilinear method
function y_1(x)
    e^(4*x)
end

function u_deriv(x, u)
    x/(u^2 * e^(12*x) * (u*e^(4*x)+1))
end

variation_of_parameters_specified_x(y_1, u_deriv, 0.1, 10, 0, 1, 1, collect(0.0:0.1:1.0))
variation_of_parameters_specified_x(y_1, u_deriv, 0.05, 20, 0, 1, 1, collect(0.0:0.1:1.0))
variation_of_parameters_specified_x(y_1, u_deriv, 0.025, 40, 0, 1, 1, collect(0.0:0.1:1.0))

# Exercise 3.1.23
function quadrature(f, a, b, n)
    sum = 0
    i = 0
    h = (b-a)/n
    while i < n
        sum = sum + h * f(a+i*h)
        i += 1
    end
    sum
end

function f(x)
    log(x)
end

function F(x)
    x*log(x)-x
end

F(10)-F(1)
quadrature(f, 1, 10, 10)
quadrature(f, 1, 10, 20)
quadrature(f, 1, 10, 40)
quadrature(f, 1, 10, 60)
quadrature(f, 1, 10, 80)
quadrature(f, 1, 10, 160)
quadrature(f, 1, 10, 320)

# c
function f(x)
    5
end

function F(x)
    5*x
end

y = F(10)-F(1)
y - quadrature(f, 1, 10, 10)
(y - quadrature(f, 1, 10, 10))/y
(y - quadrature(f, 1, 10, 20))/y
(y - quadrature(f, 1, 10, 40))/y
(y - quadrature(f, 1, 10, 60))/y
(y - quadrature(f, 1, 10, 80))/y
(y - quadrature(f, 1, 10, 160))/y
(y - quadrature(f, 1, 10, 320))/y

# d
function f(x)
    3 + 2 * x
end

function F(x)
    3*x + x^2
end

y = F(10)-F(1)
(y - quadrature(f, 1, 10, 10))/y
(y - quadrature(f, 1, 10, 20))/y
(y - quadrature(f, 1, 10, 40))/y
(y - quadrature(f, 1, 10, 60))/y
(y - quadrature(f, 1, 10, 80))/y
(y - quadrature(f, 1, 10, 160))/y
(y - quadrature(f, 1, 10, 320))/y

# section 3.2
# improved Euler's method
function improved_euler(f, h, n, x0, y0, xSelected)
    xList = collect(x0:h:(x0+h*n))
    i = 0
    y1 = y0
    while i <= n
        x0 = xList[i+1]
        k1 = f(x0, y0)
        k2 = f(x0+h, y0 + h*k1)
        y1 = y0 + h / 2 * (k1 + k2)
        if x0 in xSelected
            println(string("x" * string(i) * "=" * string(x0) * ", " * "y" * string(i) * "=" * string(y0)))
        end
        y0 = y1
        i += 1
    end
    return y1
end

# improved Euler's method with error
function improved_euler_with_R(f, h, n, x0, y0, xSelected, R)
    xList = collect(x0:h:(x0+h*n))
    i = 0
    y1 = y0
    while i <= n
        x0 = xList[i+1]
        k1 = f(x0, y0)
        k2 = f(x0+h, y0 + h*k1)
        y1 = y0 + h / 2 * (k1 + k2)
        if x0 in xSelected
            println(string("x" * string(i) * "=" * string(x0) * ", " * "y" * string(i) * "=" * string(y0) * " R = " * string(R(x0,y0))))
        end
        y0 = y1
        i += 1
    end
    return y1
end

# improved Euler's semilinear method
function improved_euler_semilinear(y_1, uDeriv, h, n, x0, y0, u0, xSelected)
    xList = collect(x0:h:(x0+h*n))
    i = 0
    while i <= n
        x0 = xList[i+1]
        y0 = u0 * y_1(x0)
        if x0 in xSelected
            println(string("x" * string(i) * "=" * string(x0) * ", " * "y" * string(i) * "=" * string(y0) * ", " * "u" * string(i) * "=" * string(u0)))
        end
        #u1 = u0 + h * uDeriv(x0, u0)
        k1 = uDeriv(x0, u0)
        k2 = uDeriv(x0+h, u0 + h*k1)
        u1 = u0 + h / 2 * (k1 + k2)
        u0 = u1
        i += 1
    end
    return y0
end

# Heun's method
function heun(f, h, n, x0, y0, xSelected)
    xList = collect(x0:h:(x0+h*n))
    i = 0
    y1 = y0
    while i <= n
        x0 = xList[i+1]
        k1 = f(x0, y0)
        k2 = f(x0+2*h/3, y0 + 2*h/3*k1)
        y1 = y0 + h / 4 * (k1 + 3*k2)
        if x0 in xSelected
            println(string("x" * string(i) * "=" * string(x0) * ", " * "y" * string(i) * "=" * string(y0)))
        end
        y0 = y1
        i += 1
    end
    return y1
end

# Midpoint method
function midpoint(f, h, n, x0, y0, xSelected)
    xList = collect(x0:h:(x0+h*n))
    i = 0
    y1 = y0
    while i <= n
        x0 = xList[i+1]
        k1 = f(x0, y0)
        k2 = f(x0+h/2, y0 + h/2*k1)
        y1 = y0 + h * k2
        if x0 in xSelected
            println(string("x" * string(i) * "=" * string(x0) * ", " * "y" * string(i) * "=" * string(y0)))
        end
        y0 = y1
        i += 1
    end
    return y1
end

# Example 3.2.1
function f(x,y)
    -2*y + x^3 * e^(-2*x)
end

improved_euler(f, 0.1, 4, 0, 1, collect(0:0.1:0.3))
improved_euler(f, 0.05, 8, 0, 1, collect(0:0.1:0.3))

# Example 3.2.4
function f(x,y)
    1 + 2*x*y
end

# improved Euler's method
improved_euler(f, 0.2, 11, 0, 3, collect(0:0.2:2.0))

# improved Euler semilinear method
function y_1(x)
    e^(x^2)
end

function u_deriv(x, u)
    e^(-x^2)
end

improved_euler_semilinear(y_1, u_deriv, 0.2, 11, 0, 3, 3, collect(0:0.2:2.0))
improved_euler_semilinear(y_1, u_deriv, 0.1, 21, 0, 3, 3, collect(0:0.2:2.0))
improved_euler_semilinear(y_1, u_deriv, 0.05, 41, 0, 3, 3, collect(0:0.2:2.0))

# Exercises
# 3.2.2
function f(x,y)
    y + (x^2+y^2)^(1/2)
end
improved_euler(f, 0.1, 4, 0, 1, collect(0:0.1:0.3))

# 3.2.5
function f(x,y)
    -x^2 * y + sin(x*y)
end
improved_euler(f, 0.2, 4, 1, π, collect(1.0:0.2:1.6))

# 3.2.7
function f(x,y)
    -2/x*y + 3/x^3 + 1
end

improved_euler(f, 0.1, 10, 1, 1, collect(1.0:0.1:2.0))

# 3.2.9
function f(x,y)
    (2*x+1)/(5*y^4+1)
end
improved_euler(f, 0.1, 10, 2, 1, collect(2.0:0.1:3.0))
improved_euler(f, 0.05, 20, 2, 1, collect(2.0:0.1:3.0))
improved_euler(f, 0.025, 40, 2, 1, collect(2.0:0.1:3.0))

function R(x,y)
    y^5 + y - x^2 - x + 4
end
improved_euler_with_R(f, 0.1, 10, 2, 1, collect(2.0:0.1:3.0), R)
improved_euler_with_R(f, 0.05, 20, 2, 1, collect(2.0:0.1:3.0), R)
improved_euler_with_R(f, 0.025, 40, 2, 1, collect(2.0:0.1:3.0), R)

# 3.2.14
# improved Euler's method
function f(x,y)
    2*y + 1/(1+x^2)
end
improved_euler(f, 0.1, 10, 2, 2, collect(2.0:0.1:3.0))

# imrpoved Euler's semilinear method
function y_1(x)
    e^(2*x)
end

function u_deriv(x,u)
    1/((1+x^2)*e^(2*x))
end
improved_euler_semilinear(y_1, u_deriv, 0.1, 10, 2, 2, 2*e^(-4), collect(2.0:0.1:3.0))

# 3.2.17
# improved Euler's method
function f(x,y)
    -y + (e^(-x)*tan(x))/x
end
improved_euler(f, 0.05, 10, 1, 0, collect(1.0:0.05:1.5))

# improved Euler's semilinear method
function y_1(x)
    e^(-x)
end

function u_deriv(x,u)
    tan(x)/x
end

improved_euler_semilinear(y_1, u_deriv, 0.05, 10, 1, 0, 0, collect(1.0:0.05:1.5))

# 3.2.23
function f(x,y)
    -2/x*y + 3/x^3 + 1
end
midpoint(f, 0.1, 10, 1, 1, collect(1.0:0.1:2.0))
heun(f, 0.1, 10, 1, 1, collect(1.0:0.1:2.0))

# 3.2.31
function quadrature(f, a, b, n)
    sum = 0
    i = 1
    h = (b-a)/n
    while i < n
        sum = sum + h * f(a+i*h)
        i += 1
    end
    sum + h/2 * (f(a)+f(b))
end

function f(x)
    5
end

function F(x)
    5*x
end

y = F(10)-F(1)
y - quadrature(f, 1, 10, 10)
(y - quadrature(f, 1, 10, 10))/y
(y - quadrature(f, 1, 10, 20))/y
(y - quadrature(f, 1, 10, 40))/y
(y - quadrature(f, 1, 10, 60))/y
(y - quadrature(f, 1, 10, 80))/y
(y - quadrature(f, 1, 10, 160))/y
(y - quadrature(f, 1, 10, 320))/y

# c
function f(x)
    3 + 2 * x
end

function F(x)
    3*x + x^2
end

y = F(10)-F(1)
(y - quadrature(f, 1, 10, 10))/y
(y - quadrature(f, 1, 10, 20))/y
(y - quadrature(f, 1, 10, 40))/y
(y - quadrature(f, 1, 10, 60))/y
(y - quadrature(f, 1, 10, 80))/y
(y - quadrature(f, 1, 10, 160))/y
(y - quadrature(f, 1, 10, 320))/y
# exact solution if f is linear

# d
function f(x)
    1 + 2*x + 3*x^2
end

function F(x)
    x + x^2 + x^3
end

y = F(10)-F(1)
(y - quadrature(f, 1, 10, 10))/y
(y - quadrature(f, 1, 10, 20))/y
(y - quadrature(f, 1, 10, 40))/y
(y - quadrature(f, 1, 10, 60))/y
(y - quadrature(f, 1, 10, 80))/y
(y - quadrature(f, 1, 10, 160))/y
(y - quadrature(f, 1, 10, 320))/y
# error decreases in O(h^3)

# section 3.2
function runge_kutta(f, h, n, x0, y0, xSelected)
    xList = collect(x0:h:(x0+h*n))
    i = 0
    y1 = y0
    while i <= n
        x0 = xList[i+1]
        k1 = f(x0, y0)
        k2 = f(x0+h/2, y0 + h/2*k1)
        k3 = f(x0+h/2, y0 + h/2*k2)
        k4 = f(x0+h, y0+h*k3)
        y1 = y0 + h/6 * (k1 + 2*k2 + 2*k3 + k4)
        if x0 in xSelected
            println(string("x" * string(i) * "=" * string(x0) * ", " * "y" * string(i) * "=" * string(y0)))
        end
        y0 = y1
        i += 1
    end
    return y1
end

function runge_kutta_semilinear(y_1, uDeriv, h, n, x0, y0, u0, xSelected)
    xList = collect(x0:h:(x0+h*n))
    i = 0
    while i <= n
        x0 = xList[i+1]
        y0 = u0 * y_1(x0)
        if x0 in xSelected
            println(string("x" * string(i) * "=" * string(x0) * ", " * "y" * string(i) * "=" * string(y0) * ", " * "u" * string(i) * "=" * string(u0)))
        end
        k1 = uDeriv(x0, u0)
        k2 = uDeriv(x0+h/2, u0 + h/2*k1)
        k3 = uDeriv(x0+h/2, u0 + h/2*k2)
        k4 = uDeriv(x0+h, u0+h*k3)
        u1 = u0 + h/6 * (k1 + 2*k2 + 2*k3 + k4)
        u0 = u1
        i += 1
    end
    return y0
end

# Example 3.3.1
function f(x,y)
    -2*y + x^3*e^(-2*x)
end

runge_kutta(f, 0.1, 3, 0, 1, collect(0.0:0.1:0.2))

# Example 3.3.4
# Runge Kutta
function f(x,y)
    2*x*y + 1
end

runge_kutta(f, 0.2, 10, 0, 3, collect(0.0:0.2:2.0))

# Runge Kutta semilinear
function y_1(x)
    e^(x^2)
end

function u_deriv(x,u)
    e^(-x^2)
end

runge_kutta_semilinear(y_1, u_deriv, 0.2, 10, 0, 3, 3, collect(0.0:0.2:2.0))

# Example 3.3.5
function z_deriv(x,z)
    (2*x-3)/(z-1)^2
end

runge_kutta(z_deriv, 0.1, 10, -1, 4, collect(-1.0:0.1:0.0))

# Exercises
# 3.3.13
# Runge Kutta
function f(x,y)
    -3*y + e^(-3*x)*(1-4*x + 3*x^2 - 4*x^3)
end
runge_kutta(f, 0.1, 10, 0, -3, collect(0.0:0.1:1.0))
runge_kutta(f, 0.05, 20, 0, -3, collect(0.0:0.1:1.0))
runge_kutta(f, 0.025, 40, 0, -3, collect(0.0:0.1:1.0))

# Runge Kutta semilinear
function y_1(x)
    e^(-3*x)
end

function u_deriv(x,u)
    -4*x^3 + 3*x^2 - 4*x + 1
end

runge_kutta_semilinear(y_1, u_deriv, 0.1, 10, 0, -3, -3, collect(0.0:0.1:1.0))
runge_kutta_semilinear(y_1, u_deriv, 0.05, 20, 0, -3, -3, collect(0.0:0.1:1.0))
runge_kutta_semilinear(y_1, u_deriv, 0.025, 40, 0, -3, -3, collect(0.0:0.1:1.0))
# the 5th derivative of u is 0, so the semilinear method yields the exact solution

# 3.3.28
function quadrature(f, a, b, n)
    sum = 0
    i = 1
    h = (b-a)/n
    while i < n
        sum = sum + h/3 * f(a+i*h) + 2*h/3 * f(a+(i-1/2)*h)
        i += 1
    end
    sum + h/6 * (f(a)+f(b)) + 2*h/3 * f(a+(n-1/2)*h)
end

# check if you're off by one bin
function f(x)
    5
end

function F(x)
    5*x
end

y = F(10)-F(1)
y - quadrature(f, 1, 10, 10)
(y - quadrature(f, 1, 10, 10))/y
(y - quadrature(f, 1, 10, 20))/y
(y - quadrature(f, 1, 10, 40))/y
(y - quadrature(f, 1, 10, 60))/y
(y - quadrature(f, 1, 10, 80))/y
(y - quadrature(f, 1, 10, 160))/y
(y - quadrature(f, 1, 10, 320))/y

# c
function f(x)
    1 + 2*x + 3*x^2 + 4*x^3
end

function F(x)
    x + x^2 + x^3 + x^4
end

y = F(10)-F(1)
(y - quadrature(f, 1, 10, 10))/y
(y - quadrature(f, 1, 10, 20))/y
(y - quadrature(f, 1, 10, 40))/y
(y - quadrature(f, 1, 10, 60))/y
(y - quadrature(f, 1, 10, 80))/y
(y - quadrature(f, 1, 10, 160))/y
(y - quadrature(f, 1, 10, 320))/y
# exact solution for cubic f, local taylor series approximation of order 3

# d
function f(x)
    1 + 2*x + 3*x^2 + 4*x^3 + 5*x^4
end

function F(x)
    x + x^2 + x^3 + x^4 + x^5
end

y = F(10)-F(1)
(y - quadrature(f, 1, 10, 10))/y
(y - quadrature(f, 1, 10, 20))/y
(y - quadrature(f, 1, 10, 40))/y
(y - quadrature(f, 1, 10, 60))/y
(y - quadrature(f, 1, 10, 80))/y
(y - quadrature(f, 1, 10, 160))/y
(y - quadrature(f, 1, 10, 320))/y
